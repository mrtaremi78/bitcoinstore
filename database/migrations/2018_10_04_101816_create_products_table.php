<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->increments('id');

            $table->tinyInteger('product_type_id');
            $table->tinyInteger('market_id')->nullable();
            $table->tinyInteger('special_service_id')->nullable();

            $table->integer('rank')->default(0);
            $table->integer('weight');
            $table->integer('off')->default(0);

            $table->string('name',25);
            $table->string('dimensions',15);
            $table->string('colors',30)->nullable();
            $table->string('gas');
            $table->string('main_description',500);
            $table->string('sub_description',1500);
            $table->string('avatar_image_path',50)->nullable();

            //1=>storeroom 2=>in stock but not in storeroom 3=>not stock
            $table->tinyInteger('status');

            $table->float('poweruse');
            $table->float('btc_per_hour');
            $table->float('price_per_unit');


            $table->SoftDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
