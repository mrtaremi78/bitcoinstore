<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('postman_id');
            $table->integer('shipmenttype_id');
            $table->integer('transaction_id');
            $table->dateTime('send_date');
            $table->dateTime('delivered_date');
            $table->string('send_address');
            $table->string('delivered_address');
            $table->string('description');
            $table->string('post_code');
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment');
    }
}
