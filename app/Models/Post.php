<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 * @package App\Models
 * @version November 19, 2018, 1:59 pm UTC
 *
 * @property string title
 * @property string content
 */
class Post extends Model
{
    use SoftDeletes;

    public $table = 'posts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'avatar_image_path',
        'user_id',
        'description',
        'content'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'title' => 'string',
        'content' => 'string',
        'avatar_image_path'=>'string',
        'description'=>'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user(){

        return $this->belongsTo('App\Models\User');

    }

    public function posttags()
    {
        return $this->belongsToMany(PostTag::class);
    }

    
}
