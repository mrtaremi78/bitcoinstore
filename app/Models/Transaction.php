<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transaction
 * @package App\Models
 * @version October 22, 2018, 1:53 pm UTC
 *
 * @property integer user_id
 * @property integer paymenttype_id
 * @property float total_prices
 * @property string status
 * @property string province_id
 * @property string city_id
 * @property string Neighbourhood_id
 */
class Transaction extends Model
{
    use SoftDeletes;

    public $table = 'transactions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'paymenttype_id',
        'total_prices',
        'status',
        'province_id',
        'city_id',
        'Neighbourhood_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'paymenttype_id' => 'integer',
        'total_prices' => 'float',
        'status' => 'string',
        'province_id' => 'string',
        'city_id' => 'string',
        'Neighbourhood_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function allsells(){

        return $this->hasMany('App\Models\Allsell');

    }

    public function shipments(){

        return $this->hasMany('App\Models\Shipment');

    }

}
