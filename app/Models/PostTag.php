<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PostTag
 * @package App\Models
 * @version November 27, 2018, 1:38 pm UTC
 *
 * @property string name
 */
class PostTag extends Model
{
    use SoftDeletes;

    public $table = 'posttags';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

}
