<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Allsell
 * @package App\Models
 * @version October 22, 2018, 2:21 pm UTC
 *
 * @property integer product_id
 * @property integer transaction_id
 * @property integer count
 */
class Allsell extends Model
{
    use SoftDeletes;

    public $table = 'allsell';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'transaction_id',
        'count'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'transaction_id' => 'integer',
        'count' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
