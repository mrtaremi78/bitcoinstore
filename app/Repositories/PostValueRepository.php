<?php

namespace App\Repositories;

use App\Models\PostValue;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PostValueRepository
 * @package App\Repositories
 * @version November 15, 2018, 7:34 am UTC
 *
 * @method PostValue findWithoutFail($id, $columns = ['*'])
 * @method PostValue find($id, $columns = ['*'])
 * @method PostValue first($columns = ['*'])
*/
class PostValueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'post_id',
        'value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PostValue::class;
    }
}
