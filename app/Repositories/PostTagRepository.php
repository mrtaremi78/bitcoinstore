<?php

namespace App\Repositories;

use App\Models\PostTag;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PostTagRepository
 * @package App\Repositories
 * @version November 27, 2018, 1:38 pm UTC
 *
 * @method PostTag findWithoutFail($id, $columns = ['*'])
 * @method PostTag find($id, $columns = ['*'])
 * @method PostTag first($columns = ['*'])
*/
class PostTagRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PostTag::class;
    }
}
