<?php

namespace App\Repositories;

use App\Models\PaymentType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PaymentTypeRepository
 * @package App\Repositories
 * @version October 16, 2018, 1:21 pm UTC
 *
 * @method PaymentType findWithoutFail($id, $columns = ['*'])
 * @method PaymentType find($id, $columns = ['*'])
 * @method PaymentType first($columns = ['*'])
*/
class PaymentTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaymentType::class;
    }
}
