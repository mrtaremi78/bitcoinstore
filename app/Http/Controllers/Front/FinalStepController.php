<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FinalStepController extends Controller
{
    public function index(){
        return view('front.finalstep');
    }
}
