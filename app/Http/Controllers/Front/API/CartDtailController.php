<?php

namespace App\Http\Controllers\Front\API;

use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;

class CartDtailController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {

        $this->productRepository = $productRepo;

    }

    public function showall(){

        $cc=explode('|',$_COOKIE['cart']);

        $products=array();

        foreach ($cc as $k => $c){

            $product = $this->productRepository->find((int)$c,[
                'id',
                'name',
                'price_per_unit',
                'avatar_image_path',
                'special_service_id',
                'off'
            ]);

            $off=(int)$product['off'];

            $price_per_unit=(int)$product['price_per_unit'];

            $product ['after_off']=round($price_per_unit-($off*$price_per_unit)/100);

            $product ['off_price']=round(($off*$price_per_unit)/100);

            array_push($products,$product);

        }

//        dd($products);

        return response()->json([

            'products'=>$products

        ]);

    }
}
