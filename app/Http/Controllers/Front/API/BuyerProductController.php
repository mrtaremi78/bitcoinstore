<?php

namespace App\Http\Controllers\Front\API;
use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Repositories\ImageRepository;
use App\Repositories\CommentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BuyerProductController extends AppBaseController
{
    /** @var  ProductRepository */

    private $productRepository;
    private $imageRepository;
    private $commentRepository;

    public function __construct(ProductRepository $productRepo,ImageRepository $imageRepository,CommentRepository $commentRepository)
    {
        $this->productRepository = $productRepo;
        $this->imageRepository=$imageRepository;
        $this->commentRepository=$commentRepository;
    }


    public function showall()
    {

        $product = $this->productRepository->orderBy('rank','dsc')->all(['id','name','main_description','status','price_per_unit','avatar_image_path',]);

//        dd($product);

//        dd($this->productModel->imageof()->where('role','=',1)[42]);

        if (empty($product)) {
            Flash::error('Product not found');

            return response()->status(404);
        }

        return response()->json([
            'products'=>$product
        ]);
    }

    public function show($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return response()->status(404);
        }

        $image_paths=$this->imageRepository->orderBy('role','asc')->findWhere([
            'product_id'=>$product['id']
        ],['path']);

        return response()->json([
            'product'=>$product,
            'images'=>$image_paths,
        ]);
    }

    public function storecomments(Request $request)
    {
        if (auth()->user()->id!=$request->user_id){
            return response('Bad request',400);
        }

        $input = $request->all();

        $this->commentRepository->create($input);

        Flash::success('Comment saved successfully.');

        return redirect(route('comments.index'));
    }

    public function showcomments($id){

        $Comments=$this->commentRepository->findWhere(

            [

                'product_id'=>$id,
                'accepted'=>'true'

        ],
            ['id','reply_of_comment','likes','value','created_at']);

        error_log($id);

        return response()->json([
            'comments'=>$Comments,
        ]);

    }

}
