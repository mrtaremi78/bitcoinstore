<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Requests\CreateShipmentTypeRequest;
use App\Http\Requests\UpdateShipmentTypeRequest;
use App\Repositories\ShipmentTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Http\Controllers\AdminPanel\CheckRoleAdmin;

class ShipmentTypeController extends AppBaseController
{
    /** @var  ShipmentTypeRepository */
    private $shipmentTypeRepository;

    public function __construct(ShipmentTypeRepository $shipmentTypeRepo)
    {
        $this->shipmentTypeRepository = $shipmentTypeRepo;
    }

    /**
     * Display a listing of the ShipmentType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $this->shipmentTypeRepository->pushCriteria(new RequestCriteria($request));
        $shipmentTypes = $this->shipmentTypeRepository->all();

        return view('shipment_types.index')
            ->with('shipmentTypes', $shipmentTypes);
    }

    /**
     * Show the form for creating a new ShipmentType.
     *
     * @return Response
     */
    public function create()
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        return view('shipment_types.create');
    }

    /**
     * Store a newly created ShipmentType in storage.
     *
     * @param CreateShipmentTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateShipmentTypeRequest $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $input = $request->all();

        $shipmentType = $this->shipmentTypeRepository->create($input);

        Flash::success('Shipment Type saved successfully.');

        return redirect(route('shipmentTypes.index'));
    }

    /**
     * Display the specified ShipmentType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $shipmentType = $this->shipmentTypeRepository->findWithoutFail($id);

        if (empty($shipmentType)) {
            Flash::error('Shipment Type not found');

            return redirect(route('shipmentTypes.index'));
        }

        return view('shipment_types.show')->with('shipmentType', $shipmentType);
    }

    /**
     * Show the form for editing the specified ShipmentType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $shipmentType = $this->shipmentTypeRepository->findWithoutFail($id);

        if (empty($shipmentType)) {
            Flash::error('Shipment Type not found');

            return redirect(route('shipmentTypes.index'));
        }

        return view('shipment_types.edit')->with('shipmentType', $shipmentType);
    }

    /**
     * Update the specified ShipmentType in storage.
     *
     * @param  int              $id
     * @param UpdateShipmentTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateShipmentTypeRequest $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $shipmentType = $this->shipmentTypeRepository->findWithoutFail($id);

        if (empty($shipmentType)) {
            Flash::error('Shipment Type not found');

            return redirect(route('shipmentTypes.index'));
        }

        $shipmentType = $this->shipmentTypeRepository->update($request->all(), $id);

        Flash::success('Shipment Type updated successfully.');

        return redirect(route('shipmentTypes.index'));
    }

    /**
     * Remove the specified ShipmentType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $shipmentType = $this->shipmentTypeRepository->findWithoutFail($id);

        if (empty($shipmentType)) {
            Flash::error('Shipment Type not found');

            return redirect(route('shipmentTypes.index'));
        }

        $this->shipmentTypeRepository->delete($id);

        Flash::success('Shipment Type deleted successfully.');

        return redirect(route('shipmentTypes.index'));
    }
}
