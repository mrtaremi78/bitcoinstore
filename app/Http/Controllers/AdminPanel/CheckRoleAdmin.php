<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckRoleAdmin extends Controller
{
    public static function CheckRoleAdmin($role_id){

        if($role_id > 1){
            return false;
        }
        else{
            return true;
        }
    }
}
