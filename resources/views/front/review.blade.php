@extends('layouts.CartMasterPage')
@section('SubCssCart')
<title>بیتکوین - بررسی</title>
    <style>
        .imgPrc {
            width: 95px;
            height: 95px;
            display: block;
            margin:0 auto;
            margin-top: 10px;
        }

        .closeBox {
            position: absolute;
            top: -5px !important;
            right: 8px;
            background-color: rgba(184, 184, 184, 0.979);
            border-radius: 50%;
            box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5);
            padding: 5px;
            cursor: pointer;
        }

        .shoppingPrc {
            background-color: #fff;
            width:auto;
            border: 1px solid #707070;
            height: auto !important;
            box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5);
            border-radius: 2px;
        }

        .nomarginTop {
            margin-top: 0px !important;
        }

        .row2 {
            margin-top: 30px !important;
        }

        .row3 {
            margin-top: 30px !important;
            line-height: 35px;
            font-size: 14px;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        a:hover {
            color: #fff;
            text-decoration: none;
        }


        #closeBox {
            width: auto;
            height: auto;
        }


        .editAddress {
            background-color: #F1F1F1;
            margin-left: 10px;
            margin-top: 7px;
            border-radius: 4px;
            width: 90px;
            text-align: center;
            cursor: pointer;
        }

        .nameOfReciever {
            margin-right: 10px;
            margin-top: 7px;
        }

        .marginRight30px {
            margin-right: 30px;
        }

        #iconCheck {
            float: right;
            background-color: #2699FB;
            color: #fff;
            position: absolute;
            border-radius: 50%;
            top: 11%;
            right: 0;
            padding: 5px;
            margin-right: 3px;

        }

        @media (max-width: 768px) {
            #iconCheck {
                top: 8%;

            }
            .noMarginRight {
                margin-right: 0px !important;
            }
        }

        .boxProduct {
            height: 250px;
            border: 1px solid #DCDCDC;
            margin-bottom: 20px;
            border-radius: 2px;

        }
        .closeBox1 {
            position: relative;
            top: -5px !important;
            right: -5px;
            background-color: rgba(184, 184, 184, 0.979);
            border-radius: 50%;
            box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5);
            padding: 5px;
            cursor: pointer;
        }

        #phoneNumber{
            border-left : 1px solid gray; padding-left: 15px; margin-left: 2px;
        }
        #postCode{
            margin-right : -15px;
        }
        .floatForLeft{
            float: right;width: 5%;
        }
        .floatForRight{
            float: right;width: 10%;
        }
        floatForRightPlus{
            float: left;width: 85%; height: 100%;
        }
        .floatForTextLeft{
            float : left;
        }
        .uk-modal-title1 {
            font-size: 1.5rem  !important;
            line-height: 1.3 !important;
            padding-right: 15px !important;
        }
        .noRadius{
            border-radius: 2px !important;
        }
    </style>
@endsection
@section('SubModalCart')
<div id="modal-close-default1" class="uk-margin-medium-top" uk-modal>

        <div class="uk-modal-dialog">
            <button class="uk-modal-close-default " type="button" uk-close></button>
            <div class="uk-modal-header">
                <p class="uk-modal-title1 ">ویرایش اطلاعات</p>
            </div>
            <form action="product.blade.php">
            <div class="uk-modal-body">
                <div class="row">
                    <div class="col-md-6 uk-margin-medium-bottom">
                        <div class="">
                            <label for="" class=""> شماره تماس: </label>
                            <input class="form-control noRadius" type="text" placeholder="0912586397" value="">
                        </div>
                    </div>
                    <div class="col-md-6 uk-margin-medium-bottom">
                        <div class="">
                            <label for="" class="">کد پستی: </label>
                            <input class="form-control noRadius" type="text" placeholder="5962292" value="">
                        </div>
                    </div>
                    <div class="col-md-12 uk-margin-medium-bottom">
                        <div class="">
                            <label for="" class=""> آدرس: </label>
                            <select class="uk-input" name="" id="">
                                    <option value="">آدرس ارسال : استان تهران ، ‌شهر تهران، محله کریمخان، تهران-خیابان کریم خان زند</option>
                                    <option value="">آدرس ارسال : استان تهران ، ‌شهر تهران، محله کریمخان، تهران-خیابان کریم خان زند</option>
                                    <option value="">آدرس ارسال : استان تهران ، ‌شهر تهران، محله کریمخان، تهران-خیابان کریم خان زند</option>
                            </select>
                         </div>

                    </div>

                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <input type="submit" class="uk-button uk-button-primary" value="ثبت">
                <input type="reset" class="uk-button uk-button-default " value="انصراف"></input>
            </div>
            </form>
        </div>

</div>
@endsection
@section('SubMainCart')

                <div class="col-md-9 uk-margin-medium-bottom">
                    <div class="shoppingPrc uk-margin-bottom">
                        <div class="iconCheck">
                            <span uk-icon="icon: check ; ratio : .8" id="iconCheck"></span>
                        </div>
                        <div class="row">

                            <div class="col-md-6 col-6">
                                <div class="marginRight30px">
                                    <p class="fontRegular nameOfReciever">گیرنده : محمد علیپور</p>
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <div>
                                    <p class="fontRegular uk-align-left editAddress" uk-toggle="target: #modal-close-default1">ویرایش</p>
                                </div>
                            </div>
                            <div class="col-md-3 col-6 ">
                                <div class="marginRight30px">
                                    <p class="fontRegular uk-align-right" id="phoneNumber">شماره
                                        تماس : <span class="uk-text-meta">0912586397</span></p>
                                </div>
                            </div>
                            <div class="col-md-3 col-6">
                                <div>
                                    <p class="fontRegular uk-align-right noMarginRight" id="postCode">کد
                                        پستی : <span class="uk-text-meta">59622929</span></p>
                                </div>
                            </div>
                            <div class="col-md-12 col-12 ">
                                <div class="marginRight30px">
                                    <p class="fontRegular">آدرس ارسال : استان تهران ، ‌شهر تهران، محله کریمخان،
                                        تهران-خیابان کریم خان زند</p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <h5 class="fontRegular">تصمیم نهایی</h5>
                    <div class="shoppingPrc uk-margin-bottom">
                        <h4 class="fontRegular text-center uk-margin-top">لیست کالاهای قابل پرداخت</h4>
                        <hr>
                        <div class="row" style="margin: 0 auto!important;">
                            <div class="col-md-4 col-12 ">
                                <div class="boxProduct uk-background-muted">
                                    <img src="img/mine.jpg" class="imgPrc" alt="">
                                    <div class="text-center">
                                        <h5 class="uk-margin-top">Z9 دستگاه ماینر مدل با فناوری
                                        </h5>
                                        <p class="uk-text-meta">
                                            تعداد : 2
                                        </p>
                                        <p>
                                           <span class="text-primary"> 598,000</span> تومان
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12 ">
                                <div class="boxProduct uk-background-muted">
                                    <img src="img/mine.jpg" class="imgPrc" alt="">
                                    <div class="text-center">
                                        <h5 class="uk-margin-top">Z9 دستگاه ماینر مدل با فناوری
                                        </h5>
                                        <p class="uk-text-meta">
                                            تعداد : 2
                                        </p>
                                        <p>
                                            <span class="text-primary"> 598,000</span> تومان
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12 ">
                                <div class="boxProduct uk-background-muted">
                                    <img src="img/mine.jpg" class="imgPrc" alt="">
                                    <div class="text-center">
                                        <h5 class="uk-margin-top">Z9 دستگاه ماینر مدل با فناوری
                                        </h5>
                                        <p class="uk-text-meta">
                                            تعداد : 2
                                        </p>
                                        <p>
                                            <span class="text-primary"> 598,000</span> تومان
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection
@section('SubScriptCart')
@endsection


    
            




