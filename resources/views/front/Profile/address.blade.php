<!doctype html>
<html lang="fa" dir="rtl">

<head>
    <title>2بیتکوین</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="img/coin_Tg0_icon.ico" type="image/x-icon">

    <!-- en font -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <!-- Bootstrap rtl CSS -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css" integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz"
        crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/css/uikit.min.css" />

    <style>
       @font-face {
    font-family: "IRANSansWeb";
    src: url(fonts/IRANSansWeb.ttf);
}
body {

    font-family: "IRANSansWeb";
    /* background-image: url('/img/bit61.png');
    background-repeat: no-repeat;
    background-size: cover;
    background-attachment: fixed; */
    
  }
  
  h1,h2,h3,h4,h5,h6,h7{
      font-family: "IRANSansWeb";
  }
        .inputx{
        border-radius:3px;
        border: 1px solid rgba(0, 0, 0, 0.434);
        box-shadow: 0px 1px 5px #00000020 !important;
    }
    #btnx{
        border-radius:3px;
        width: 280px;
    }
    #btnx span{
        font-size: 1.2rem;
        line-height: 10px;
    }
    .cancelx{
        margin-bottom: 0px;
        padding-bottom: 30px;
    }
    #headerTitle{
        width: 100%;
        height: 120px;
        border-bottom: 1px solid rgba(0, 0, 0, 0.558);
        box-shadow: 0px 3px 15px #00000040 !important;
    }
    #headerTitle p{
        text-align: center;
        line-height: 0px;
        font-size: 12px;
    }
    #headerTitle h1{
        text-align: center;
        padding-top: 30px;
        line-height: 30px;
        color: #FFD700;
    }
    .addresses{
         width: 100%;
         height: auto;
   }


    .address{
        box-shadow: 0 2px 8px rgba(0, 0, 0, 0.16);
        border: 1px solid gray;
        width: 100%;
        height: auto;
        min-height: 200px;
        position: relative;
        transition: 500ms;
        cursor: pointer;
        color : black;
        
    }
    a{
        text-decoration: none!important;  
    }
    .address .boxAddress{
        position: absolute;
        margin: 0;
        top: 0;
        left:0;
        z-index: 110;
        height:100%;
        transition: 500ms;
    }
    .address:hover .boxAddress{
        filter: blur(5px);
        z-index: 80;
    }
    .address .overlay{
        position: absolute;
        top: 0;
        left:0;
        width: 100%;
        height:200px;
        background-color: rgba(78, 78, 78, 0.306);
        z-index: 100;
        transition: 500ms;  
    }
    .address .overlay p{
        line-height: 200px;
    }
    .eAddress{
        margin-top:10px;
    }
    #editOnPhone{
        display: none;
    }
    @media (max-width: 768px) {
        .address:hover .boxAddress{
        filter: blur(0);
        z-index: 80;
        }
   
    .address .overlay{
        display: none;
        }
        .address .boxAddress{
            position: static;
            height: auto;
        }
        #editOnPhone{
        display: block;
    }
    }       
    </style>


</head>

<body class="uk-background-muted">




    <div id="headerTitle" class="">
        <h1 style="">ای بیت</h1>
        <p>هر ایرانی یک ماینر</p>
    </div>
    <div class="container-fluid">
            <div class="addresses">
                    <div id="editOnPhone" class="alert alert-dark uk-margin-top text-center" role="alert">
                           برای ویرایش برروی آدرس کلیک کنید
                          </div>
                        <a href="#inputAddress" uk-scroll>

                                <div class="address uk-background-default uk-margin-top ">
                
                                        <div class="row text-center uk-background-default boxAddress">
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">نام : </span><span id="nameAddress">محمد</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">نام خانوادگی : </span><span id="lnameAddress">طارمی</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">تلفن همراه : </span><span id="phoneAddress">09xxxxxx</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">تلفن ثابت : </span><span id="faxAddress">021xxxxxx</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">
                            
                                                            کد پستی : </span><span id="codeAddress">8239772531</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">استان : </span><span id="provinceAddress">تهران</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">شهر : </span><span id="cityAddress">تهران</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">
                            
                                                            محله:</span><span id="parishAddress">تهران</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-12 eAddress">
                                                <div class="text-center">
                                                    <p><span class="uk-text-meta uk-text-small">آدرس : </span><span id="theAddress">استان تهران ،شهر تهران، محله کریمخان، تهران-خیابان کریم خان زند</span></p>
                                                </div>
                                            </div>
                            
                                        </div>
                                        <div class="overlay">
                                        
                                            <p class="uk-text-large text-center">برای تغییر کلیک کنید <span class="" uk-icon="icon: pencil;ratio: 1.7"></span></p>
                                        </div>
                                    </div>

                        </a>
                        <a href="#inputAddress" uk-scroll>

                                <div class="address uk-background-default uk-margin-top ">
                
                                        <div class="row text-center uk-background-default boxAddress">
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">نام : </span><span id="nameAddress">محمد</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">نام خانوادگی : </span><span id="lnameAddress">طارمی</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">تلفن همراه : </span><span id="phoneAddress">09xxxxxx</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">تلفن ثابت : </span><span id="faxAddress">021xxxxxx</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">
                            
                                                            کد پستی : </span><span id="codeAddress">8239772531</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">استان : </span><span id="provinceAddress">تهران</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">شهر : </span><span id="cityAddress">تهران</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <div class="eAddress">
                                                    <p><span class="uk-text-meta uk-text-small">
                            
                                                            محله:</span><span id="parishAddress">تهران</span></p>
                                                </div>
                                            </div>
                                            <div class="col-md-12 eAddress">
                                                <div class="text-center">
                                                    <p><span class="uk-text-meta uk-text-small">آدرس : </span><span id="theAddress">استان تهران ،شهر تهران، محله کریمخان، تهران-خیابان کریم خان زند</span></p>
                                                </div>
                                            </div>
                            
                                        </div>
                                        <div class="overlay">
                                        
                                            <p class="uk-text-large text-center">برای تغییر کلیک کنید <span class="" uk-icon="icon: pencil;ratio: 1.7"></span></p>
                                        </div>
                                    </div>

                        </a>
                </div>
    </div>

    <div class="container">

        <br>
        <br>
        <h3 class="uk-text-muted" id="inputAddress"><span uk-icon="icon: plus" class="uk-margin-small-left uk-text-primary"></span>افزودن
            آدرس تحویل گیرنده</h3>
        <form action="">
            <div class="row">

                <div class="col-md-6">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">نام:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <input maxlength="25" class="form-control noRadius uk-form-large inputx" 
                                type="text" placeholder="" id="nameAddressI">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">نام خانوادگی:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <input maxlength="25" class="form-control noRadius  uk-form-large inputx" 
                                type="text" placeholder="" id="lnameAddressI">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text"> تلفن همراه:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <input maxlength="12" class="form-control noRadius  uk-form-large inputx" 
                                type="tel" placeholder="" id="phoneAddressI">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">تلفن ثابت:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <input maxlength="12" class="form-control noRadius  uk-form-large inputx"
                                type="text" placeholder="" id="faxAddressI">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">کد پستی:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <input maxlength="20" class="form-control noRadius  uk-form-large inputx"
                                type="text" placeholder="" id="codeAddressI">
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">استان:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <select class="custom-select form-control noRadius uk-form-large inputx" id="provinceAddressI">
                                <option>تهران</option>
                                <option>تبریز</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">شهر:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <select class="custom-select form-control noRadius uk-form-large inputx" id="cityAddressI">
                                <option>تهران</option>
                                <option>تبریز</option>
                            </select>
                        </div>



                    </div>
                </div>
                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">محله:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <select class="custom-select form-control noRadius uk-form-large inputx" id="parishAddressI">
                                <option>تهران</option>
                                <option>تبریز</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">آدرس:</label><span class="uk-text-danger">*</span>
                        <textarea maxlength="200" class="uk-textarea inputx" rows="5" placeholder="" id="theAddressI"></textarea>
                    </div>
                </div>

                <div class="col-md-12">

                </div>

            </div>
            <button class="uk-button uk-button-primary uk-button-large uk-align-center" id="btnx"><span class="">ثبت
                    آدرس</span></button>
            <a href="/profile" class="uk-align-center uk-text-center uk-text-muted cancelx"> انصراف و بازگشت</a>
        </form>
    </div>




    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://cdn.rtlcss.com/bootstrap/v4.0.0/js/bootstrap.min.js" integrity="sha384-54+cucJ4QbVb99v8dcttx/0JRx4FHMmhOWi4W+xrXpKcsKQodCBwAvu3xxkZAwsH"
        crossorigin="anonymous"></script>

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit-icons.min.js"></script>

    <!-- script -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function () {
            $(".address").click(function () {
                $("#nameAddressI").val($(this).find("#nameAddress").text()) ;
                $("#lnameAddressI").val($(this).find("#lnameAddress").text()) ;
                $("#phoneAddressI").val($(this).find("#phoneAddress").text()) ;
                $("#faxAddressI").val($(this).find("#faxAddress").text()) ;
                $("#codeAddressI").val($(this).find("#codeAddress").text()) ;
                $("#provinceAddressI").val($(this).find("#provinceAddress").text()) ;
                $("#cityAddressI").val($(this).find("#cityAddress").text()) ;
                $("#parishAddressI").val($(this).find("#parishAddress").text()) ;
                $("#theAddressI").val($(this).find("#theAddress").html()) ;
            })
                    $("#editOnPhone").click(function () {
    
                        UIkit.notification({message: '<center>بر روی آدرس کلیک کنید!!</center>', status: 'warning'})
                    })
        })
    </script>
</body>

</html>