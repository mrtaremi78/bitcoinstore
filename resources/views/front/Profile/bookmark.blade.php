@extends('layouts.ProfileMasterPage')

@section('SubCssProfile')
<title>بیتکوین - تغییر رمزعبور</title>
<style>
    a{
        text-decoration: none !important;
    }
    .bookmarkFullbox{
        height: auto;
        border: 1px solid gray;
        border-radius: 5px;
        background-color: #fff;
    }
    .bookmarkFullbox .bookmarkImg img{
        width: 120px;
        height: auto;
        padding: 10px;
        float: right;
    }
    .deviceName{
        display: inline-block;
        margin: 0 !important;
        line-height: 60px;
        margin-right: 10px !important;
        font-size: 16px;
    }
    .price {
        display: inline;
        margin: 0 !important;
        margin-right: 12px !important;
        font-size: 16px;
        line-height:60px;
    }
    .power{
        display: inline;
        /* margin: 0 !important; */
        margin-right: 40px !important;
        font-size: 16px;
        line-height: 40px;
    }
    .bookmarkFullbox button{
        /* margin-left: 10px; */
        /* float: left; */
        /* margin-top: -28px; */
        border-top-left-radius: 0 !important;
        border-top-right-radius: 0 !important;
        border-bottom-left-radius: 3px !important;
        border-bottom-right-radius: 3px !important;
        background-color: #ff9f43 ;
        border: 3px solid #ff9f43;
    }
</style>
@endsection

@section('SubModalProfile')


@endsection




@section('SubMainProfile')
<p class=" uk-margin-top uk-margin-right">لیست مورد علاقه ها</p>
<div class="col-md-12 uk-margin-medium-bottom">
    <div class="bookmarkFullbox">
        <div class="bookmarkImg">
            <img src="/img/mine.jpg" alt="">
            <p class="deviceName">دستگاه ماینر ANTMINE S8</p><br>
            <p class="price">قیمت : 4,000,000 تومان</p>
            <span class="power">هش/ریت : 120 </span>
            <span class="power">مصرف برق : 53Q </span>
            <span class="power">قدرت پردازش : 25W</span>
        </div>
        <a href="/calculations">
            <button class="btn btn-primary uk-remove-margin btn-block uk-margin-top">محاسبه درآمد این محصول</button>
        </a>
    </div>    
</div>
<div class="col-md-12 uk-margin-medium-bottom">
        <div class="bookmarkFullbox">
            <div class="bookmarkImg">
                <img src="/img/mine.jpg" alt="">
                <p class="deviceName">دستگاه ماینر ANTMINE S8</p><br>
                <p class="price">قیمت : 4,000,000 تومان</p>
                <span class="power">هش/ریت : 120 </span>
                <span class="power">مصرف برق : 53Q </span>
                <span class="power">قدرت پردازش : 25W</span>
            </div>
            <a href="/calculations">
                <button class="btn btn-primary uk-remove-margin btn-block uk-margin-top">محاسبه درآمد این محصول</button>
            </a>
        </div>    
    </div>
    <div class="col-md-12 uk-margin-medium-bottom">
            <div class="bookmarkFullbox">
                <div class="bookmarkImg">
                    <img src="/img/mine.jpg" alt="">
                    <p class="deviceName">دستگاه ماینر ANTMINE S8</p><br>
                    <p class="price">قیمت : 4,000,000 تومان</p>
                    <span class="power">هش/ریت : 120 </span>
                    <span class="power">مصرف برق : 53Q </span>
                    <span class="power">قدرت پردازش : 25W</span>
                </div>
                <a href="/calculations">
                    <button class="btn btn-primary uk-remove-margin btn-block uk-margin-top">محاسبه درآمد این محصول</button>
                </a>
            </div>    
        </div>

@endsection


@section('SubScriptProfile')
<script>
    $(document).ready(function () {
        checkcart();
        function checkcart() {
            if ($(".bookmarkBox").children().length == 0) {
                $(".bookmarkBox").append("<div style='margin: 150px auto; opacity:0.9'><div style='margin: 0 auto; width : 90px;height:90px;' > <svg aria-hidden='true' data-prefix='fal' data-icon='bookmark' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 384 512' class='svg-inline--fa fa-bookmark fa-w-12 fa-5x'><path fill='rgb(153,153,153)' d='M336 0H48C21.49 0 0 21.49 0 48v464l192-112 192 112V48c0-26.51-21.49-48-48-48zm16 456.287l-160-93.333-160 93.333V48c0-8.822 7.178-16 16-16h288c8.822 0 16 7.178 16 16v408.287z' ></path></svg> </div> <p class='text-center uk-margin-medium-top uk-text-meta'>محصولی در لیست علاقه مندی شما وجود ندارد</p></div>");
            }
        }
        $(".deleteBookmark").click(function () {
            $(this).parent().parent().hide("slide", { direction: "right" }, 500, function () {
                $(this).remove();
                checkcart();
            });
        });

    });
</script>
@endsection