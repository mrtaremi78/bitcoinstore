@extends('layouts.ProfileMasterPage') 

@section('SubCssProfile')
<title>بیتکوین - تغییر رمزعبور</title>
<style>
body{
 background-image: url('/img/bit61.png');
  background-repeat: no-repeat;
  background-size: cover;
  background-attachment: fixed;
}

.imgg{
    height: 80px !important;
    width: 80px !important;
    background-image: url("/img/svg/lock.svg");
    margin: 20px auto;
    background-repeat: no-repeat;
    background-size: covesr;
    transition: 1000ms;
    opacity: 0.5;
    /* overflow: scroll; */
}
/* .imgg{
    
    background-repeat: no-repeat;
} */

.imgg:hover{
    opacity: 0.7;
    /* transform: scale(1.2); */
    
}
</style>

@endsection

@section('SubModalProfile')


@endsection




@section('SubMainProfile')
        <!-- <img class="bg" src="/img/bit.jpg" alt=""> -->
        <div class="col-md-8 offset-md-2 ">
            <br>
            <br>
            <br>
            <h4>تغییر رمز عبور</h4>
            <div style="border : 1px solid gray; padding: 10px;box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5); border-radius: 5px;   background-color: white; opacity: 0.9;">

                <div class="imgg">
                    <!-- <img src="" width="50" height="50" alt="5555"> -->
                </div>
                    <div class="uk-margin">
                    <input type="password" class="uk-input" placeholder="رمز عبور فعلی">
                    </div>
                    <div class="uk-margin">
                    <input type="password" class="uk-input" placeholder="رمز عبور جدید">
                    </div>
                    <div class="uk-margin">
                    <input type="password" class="uk-input" placeholder="تکرار رمز عبور">
                    </div>
                    <div class="uk-margin">
                    <input type="submit" value="ثبت تغیرات" class="btn btn-primary uk-width-1-1 uk-margin-small-bottom">
                    </div>
                    
                </div>
            </div>

@endsection


@section('SubScriptProfile')
<script>
    $(document).ready(function () {
        $(".imgg").css("transform","scale(1.3)");
    });
</script>
@endsection