<!doctype html>
<html lang="fa" dir="rtl">

<head>
    <title>1بیتکوین</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="img/coin_Tg0_icon.ico" type="image/x-icon">

    <!-- en font -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <!-- Bootstrap rtl CSS -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css" integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz"
        crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/css/uikit.min.css" />

    <style>
    .inputx{
        border-radius:3px;
        border: 1px solid rgba(0, 0, 0, 0.434);
        box-shadow: 0px 1px 5px #00000020 !important;
    }
    #btnx{
        border-radius:3px;
        width: 280px;
    }
    #btnx span{
        font-size: 1.2rem;
        line-height: 10px;
    }
    .cancelx{
        margin-bottom: 0px;
        padding-bottom: 30px;
    }
    #headerTitle{
        width: 100%;
        height: 120px;
        border-bottom: 1px solid rgba(0, 0, 0, 0.558);
        box-shadow: 0px 3px 15px #00000040 !important;
    }
    #headerTitle p{
        text-align: center;
        line-height: 0px;
        font-size: 12px;
    }
    #headerTitle h1{
        text-align: center;
        padding-top: 30px;
        line-height: 30px;
        color: #FFD700;
    }
    </style>


</head>

<body class="uk-background-muted">

    
    

    <div id="headerTitle" class="">
        <h1 style="">ای بیت</h1>
        <p>هر ایرانی یک ماینر</p>
    </div>
    <div class="container">

        <br>
    <br>
        <h3 class="uk-text-muted">
            <span uk-icon="icon: plus" class="uk-margin-small-left uk-text-primary"></span>افزودن آدرس تحویل گیرنده</h3>
        <form action="">
            <div class="row">

                <div class="col-md-6">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">نام:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <input maxlength="25" class="form-control noRadius  uk-form-large inputx" id="form-stacked-text" type="text" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">نام خانوادگی:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <input maxlength="25" class="form-control noRadius  uk-form-large inputx" id="form-stacked-text" type="text" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text"> تلفن همراه:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <input maxlength="12" class="form-control noRadius  uk-form-large inputx" id="form-stacked-text" type="tel" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">تلفن ثابت:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <input maxlength="12" class="form-control noRadius  uk-form-large inputx" id="form-stacked-text" type="text" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">کد پستی:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <input maxlength="20" class="form-control noRadius  uk-form-large inputx" id="form-stacked-text" type="text" placeholder="">
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">استان:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <select class="custom-select form-control noRadius uk-form-large inputx" id="form-stacked-select">
                                <option>تهران</option>
                                <option>تبریز</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">شهر:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <select class="custom-select form-control noRadius uk-form-large inputx" id="form-stacked-select">
                                <option>تهران</option>
                                <option>تبریز</option>
                            </select>
                        </div>



                    </div>
                </div>
                <div class="col-md-4">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">محله:</label><span class="uk-text-danger">*</span>
                        <div class="uk-form-controls">
                            <select class="custom-select form-control noRadius uk-form-large inputx" id="form-stacked-select">
                                <option>تهران</option>
                                <option>تبریز</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-text">آدرس:</label><span class="uk-text-danger">*</span>
                        <textarea maxlength="200" class="uk-textarea inputx" rows="5" placeholder=""></textarea>
                    </div>
                </div>

                <div class="col-md-12">

                </div>

            </div>
            <button class="uk-button uk-button-primary uk-button-large uk-align-center" id="btnx"><span class="">ثبت آدرس</span></button>
            <a href="#" class="uk-align-center uk-text-center uk-text-muted cancelx"> انصراف و بازگشت</a>
        </form>
    </div>




    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://cdn.rtlcss.com/bootstrap/v4.0.0/js/bootstrap.min.js" integrity="sha384-54+cucJ4QbVb99v8dcttx/0JRx4FHMmhOWi4W+xrXpKcsKQodCBwAvu3xxkZAwsH"
        crossorigin="anonymous"></script>

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit-icons.min.js"></script>

    <!-- script -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</body>

</html>