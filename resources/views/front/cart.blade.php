@extends('layouts.CartMasterPage') @section('SubCssCart')
<title>بیتکوین - سبد خرید</title>
<style>
    .imgPrc {
        width: 92px;
        height: 90px;
        margin-left: 5px;
        margin-top: 30px;
        margin-right: 15px;
        display: block;
        float: right;
    }

    .imgPrcForDelete {
        width: 92px;
        height: 92px;
        display: block;
    }

    .closeBox1 {
        position: absolute;
        top: -5px !important;
        right: 8px;
        background-color: rgba(184, 184, 184, 0.979);
        border-radius: 50%;
        box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5);
        padding: 5px;
        cursor: pointer;
    }

    .shoppingPrc {
        background-color: #fff;
        border: 1px solid #707070;
        height: auto !important;
        box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5);
        border-radius: 2px;
    }


    .nomarginTop {
        margin-top: 0px !important;
    }

    .row2 {
        margin-top: 30px !important;
    }

    .row3 {
        margin-top: 30px !important;
        line-height: 35px;
        font-size: 14px;
    }

    .textS .row1 p {
        margin-top: 25px;
        font-size: 13.5px;
        line-height: 35px;
        /* margin-right: 5px;  */
        /* margin-left: 20px; */
        /* padding-right: 160px;  */
        /* float: right; */
    }

    @media (max-width: 768px) {
        .row1 p {
            text-align: center;
        }
    }









    .cardDetails hr {
        margin: 0 auto;
        padding: 15px;
        padding-bottom: 0px !important;
    }

    a {
        color: #fff;
        text-decoration: none;
    }

    a:hover {
        color: #fff;
        text-decoration: none;
    }



    #iconFlash {
        float: right;

    }

    #closeBox {
        width: auto;
        height: auto;
    }

    .boxForImgAndText {
        width: 80%;
        height: 92px;
        margin: 0 auto;
        /* background-color: red; */
    }

    .boxForImgAndText p {
        line-height: 41px;
        margin-right: 20px;
    }

    .boxForImgAndText img,
    .boxForImgAndText p {
        float: right;
    }
</style>
@endsection @section('SubModalCart') @endsection @section('SubMainCart')
<div class="col-md-9 uk-margin-medium-bottom" id="cart">
    {{--<div class="shoppingPrc uk-margin-bottom" id="c1">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-2 col-6 col-sm-2">--}}
                {{--<div>--}}
                    {{--<div class="fontRegular" uk-tooltip="title: حذف محصول; pos: bottom">--}}
                        {{--<a uk-icon="icon: close ; ratio : 0.5" class="closeBox1 dltpost" uk-toggle></a>--}}
                    {{--</div>--}}
                    {{--<div class="closeAndImg">--}}
                        {{--<img src="img/avatar.jpg" alt="miner" class="imgPrc">--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}
            {{--<div class="col-md-4 col-6 col-sm-4">--}}
                {{--<div class="textS">--}}
                    {{--<div class="row1">--}}
                        {{--<p class="">--}}
                            {{--دستگاه ماینر پیشنهادی--}}
                            {{--<br> فروشگاه : iCrypt--}}
                            {{--<br> سرویس ویژه آی کریپت : 7 روز تضمین تعویض کالا</p>--}}
                    {{--</div>--}}
                    {{--<!-- <br> -->--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-2 col-6 col-sm-2">--}}
                {{--<div class="row2 text-center">--}}
                    {{--<p class="fontRegular">تعداد</p>--}}
                    {{--<select id="c12" class="uk-select uk-form-width-xsmall">--}}
                        {{--<option value="1">1</option>--}}
                        {{--<option value="2">2</option>--}}
                        {{--<option value="3">3</option>--}}
                        {{--<option value="4">4</option>--}}
                        {{--<option value="5">5</option>--}}
                        {{--<option value="6">6</option>--}}
                        {{--<option value="7">7</option>--}}
                        {{--<option value="8">8</option>--}}
                        {{--<option value="9">9</option>--}}
                        {{--<option value="10">10</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4 col-6 col-sm-4">--}}
                {{--<div class="textS">--}}
                    {{--<div class="row3">--}}
                        {{--<p class="fontRegular text-center">--}}
                            {{--<span class="uk-text-meta">--}}
                                {{--<del>2,582,601</del>--}}
                            {{--</span>--}}
                            {{--<br> تخفیف فروش ویژه: ۴,۶۰۰ تومان--}}
                            {{--<br> 2,400,00 تومان--}}
                        {{--</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="shoppingPrc uk-margin-bottom" id="c1">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-2 col-6 col-sm-2">--}}
                {{--<div>--}}
                    {{--<div class="fontRegular" uk-tooltip="title: حذف محصول; pos: bottom">--}}
                        {{--<a uk-icon="icon: close ; ratio : 0.5" class="closeBox1 dltpost" uk-toggle></a>--}}
                    {{--</div>--}}
                    {{--<div class="closeAndImg">--}}
                        {{--<img src="img/avatar.jpg" alt="miner" class="imgPrc">--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}
            {{--<div class="col-md-4 col-6 col-sm-4">--}}
                {{--<div class="textS">--}}
                    {{--<div class="row1">--}}
                        {{--<p class="">--}}
                            {{--دستگاه ماینر پیشنهادی--}}
                            {{--<br> فروشگاه : iCrypt--}}
                            {{--<br> سرویس ویژه آی کریپت : 7 روز تضمین تعویض کالا</p>--}}
                    {{--</div>--}}
                    {{--<!-- <br> -->--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-2 col-6 col-sm-2">--}}
                {{--<div class="row2 text-center">--}}
                    {{--<p class="fontRegular">تعداد</p>--}}
                    {{--<select id="c22" class="uk-select uk-form-width-xsmall">--}}
                        {{--<option value="1">1</option>--}}
                        {{--<option value="2">2</option>--}}
                        {{--<option value="3">3</option>--}}
                        {{--<option value="4">4</option>--}}
                        {{--<option value="5">5</option>--}}
                        {{--<option value="6">6</option>--}}
                        {{--<option value="7">7</option>--}}
                        {{--<option value="8">8</option>--}}
                        {{--<option value="9">9</option>--}}
                        {{--<option value="10">10</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4 col-6 col-sm-4">--}}
                {{--<div class="textS">--}}
                    {{--<div class="row3">--}}
                        {{--<p class="fontRegular text-center">--}}
                            {{--<span class="uk-text-meta">--}}
                                {{--<del>2,582,601</del>--}}
                            {{--</span>--}}
                            {{--<br> تخفیف فروش ویژه: ۴,۶۰۰ تومان--}}
                            {{--<br> 2,400,00 تومان--}}
                        {{--</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="shoppingPrc uk-margin-bottom" id="c1">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-2 col-6 col-sm-2">--}}
                {{--<div>--}}
                    {{--<div class="fontRegular" uk-tooltip="title: حذف محصول; pos: bottom">--}}
                        {{--<a uk-icon="icon: close ; ratio : 0.5" class="closeBox1 dltpost" uk-toggle></a>--}}
                    {{--</div>--}}
                    {{--<div class="closeAndImg">--}}
                        {{--<img src="img/avatar.jpg" alt="miner" class="imgPrc">--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}
            {{--<div class="col-md-4 col-6 col-sm-4">--}}
                {{--<div class="textS">--}}
                    {{--<div class="row1">--}}
                        {{--<p class="">--}}
                            {{--دستگاه ماینر پیشنهادی--}}
                            {{--<br> فروشگاه : iCrypt--}}
                            {{--<br> سرویس ویژه آی کریپت : 7 روز تضمین تعویض کالا</p>--}}
                    {{--</div>--}}
                    {{--<!-- <br> -->--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-2 col-6 col-sm-2">--}}
                {{--<div class="row2 text-center">--}}
                    {{--<p class="fontRegular">تعداد</p>--}}
                    {{--<select id="c32" class="uk-select uk-form-width-xsmall">--}}
                        {{--<option value="1">1</option>--}}
                        {{--<option value="2">2</option>--}}
                        {{--<option value="3">3</option>--}}
                        {{--<option value="4">4</option>--}}
                        {{--<option value="5">5</option>--}}
                        {{--<option value="6">6</option>--}}
                        {{--<option value="7">7</option>--}}
                        {{--<option value="8">8</option>--}}
                        {{--<option value="9">9</option>--}}
                        {{--<option value="10">10</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4 col-6 col-sm-4">--}}
                {{--<div class="textS">--}}
                    {{--<div class="row3">--}}
                        {{--<p class="fontRegular text-center">--}}
                            {{--<span class="uk-text-meta">--}}
                                {{--<del>2,582,601</del>--}}
                            {{--</span>--}}
                            {{--<br> تخفیف فروش ویژه: ۴,۶۰۰ تومان--}}
                            {{--<br> 2,400,00 تومان--}}
                        {{--</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}
    {{--</div>--}}


</div>























@endsection @section('SubScriptCart')
<script>
    $(document).ready(function () {


        var Products = new Map();
        var Product_count = new Map();

        var total = 0;
        var off = 0;

        var count = 0;


        function checkcart() {
            if ($("#cart").children().length == 0) {
                $("#cart").append("<img src='img/svg/danger.svg' style='display: block; margin: 50px auto;' width='100' height='100'><h4 class='text-center uk-text-meta'>محصولی در سبد خرید شما وجود ندارد</h4></div>");
            }
        }
        if (checkCookie('cart')) {
            $.get("/cartdtail", function (data, status) {


                $.each(data.products, function (i, e) {
                    $("#cart").append("<div class='shoppingPrc uk-margin-bottom' id='" + e.id + "'><div class='row'><div class='col-md-2 col-6 col-sm-2'><div><div class='fontRegular' uk-tooltip='title: حذف محصول; pos: bottom'><a uk-icon='icon: close ; ratio : 0.5' class='closeBox1 dltpost' uk-toggle></a></div><div class='closeAndImg'><img src='" + e.avatar_image_path + "' alt='miner' class='imgPrc'></div></div></div><div class='col-md-4 col-6 col-sm-4'><div class='textS'><div class='row1'><p class=''>" + e.name + "<br> فروشگاه : iCrypt<br> سرویس ویژه آی کریپت : 7 روز تضمین تعویض کالا</p></div></div></div><div class='col-md-2 col-6 col-sm-2'><div class='row2 text-center'><p class='fontRegular'>تعداد</p><select class='uk-select uk-form-width-xsmall'><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></div></div><div class='col-md-4 col-6 col-sm-4'><div class='textS'><div class='row3'><p class='fontRegular text-center'><span class='uk-text-meta'><del>" + colon(e.price_per_unit.toString()) + "</del></span><br> " + ' ' + "تخفیف فروش ویژه: "+"<span id='off_price'>"+ colon(e.off_price.toString()) +"</span>" + "<br>" +"<span id='after_off'>"+ colon(e.after_off.toString()) +"</span>" + " تومان" + "</p></div></div></div></div></div>");

                    Products.set(e.id,[e.after_off,e.off_price,e.price_per_unit]);
                    Product_count.set(e.id,1);
                    count++;
                    off+=e.off_price;
                    total+=e.price_per_unit;
                });

                var cash = total - off;
                $("#total").text(colon(total.toString()));
                $("#count").text(count);
                $("#off").text(colon(off.toString()));
                $("#cash").text(colon(cash.toString()));

                $(".uk-select").change(function () {

                    var selectcount= $(this).val();

                    var after_off= $(this).parent().parent().next().children().children().children().find('#after_off').text();
                    var off_price= $(this).parent().parent().next().children().children().children().find('#off_price').text();

                    after_off=after_off.replace(new RegExp(',', 'g'), '');
                    off_price=off_price.replace(new RegExp(',', 'g'), '');

                    var first_number = Product_count.get(parseInt($(this).parent().parent().parent().parent().attr('id')));

                    // alert(first_number.toString());

                    if((first_number-selectcount)<0){

                        total+=(selectcount-first_number)*Products.get(parseInt($(this).parent().parent().parent().parent().attr('id')))[2];
                        off+=(selectcount-first_number)*Products.get(parseInt($(this).parent().parent().parent().parent().attr('id')))[1];
                        cash = total - off;
                        count+=(selectcount-first_number);

                        $("#total").text(colon(total.toString()));
                        $("#count").text(count);
                        $("#off").text(colon(off.toString()));
                        $("#cash").text(colon(cash.toString()));

                    }

                    if((first_number-selectcount)>0){

                        total-=(first_number-selectcount)*Products.get(parseInt($(this).parent().parent().parent().parent().attr('id')))[2];
                        off-=(first_number-selectcount)*Products.get(parseInt($(this).parent().parent().parent().parent().attr('id')))[1];
                        cash = total - off;
                        count-=(first_number-selectcount);

                        $("#total").text(colon(total.toString()));
                        $("#count").text(count);
                        $("#off").text(colon(off.toString()));
                        $("#cash").text(colon(cash.toString()));

                    }

                    Product_count.set(parseInt($(this).parent().parent().parent().parent().attr('id')),selectcount);


                });

                $(".dltpost").click(function () {
                    $(this).parent().parent().parent().parent().parent().hide("slide", { direction: "up" }, 500, function () {
                        $(this).remove();
                        checkcart();
                        c = getCookie('cart');
                        cc = c.split('|');
                        var index = cc.indexOf($(this).attr("id"));
                        if (index > -1) {
                            if (cc.length == 1) {
                                setCookie('cart', '', 0);

                                $(".badgeCustom ").text(0);

                            }
                            else if (cc.length == 2) {
                                cc.splice(index, 1);
                                $(".badgeCustom ").text(cc.length);
                                setCookie('cart', cc[0], 10);
                            }
                            else {
                                cc.splice(index, 1);
                                $(".badgeCustom ").text(cc.length);
                                newcookie = cc[0];
                                for (i = 1; i < cc.length - 1; i++) {
                                    newcookie += '|' + cc[i];
                                }
                                newcookie += '|' + cc[cc.length - 1];
                                setCookie('cart', newcookie, 10);
                            }
                        }

                        var first_number = Product_count.get(parseInt($(this).attr("id")));

                        total=total-(first_number*Products.get(parseInt($(this).attr("id")))[2]);
                        off=off-(first_number*Products.get(parseInt($(this).attr("id")))[1]);
                        cash = total - off;
                        count=count-first_number;

                        $("#total").text(colon(total.toString()));
                        $("#count").text(count);
                        $("#off").text(colon(off.toString()));
                        $("#cash").text(colon(cash.toString()));

                    });

                });

            });
        }
        else {
            checkcart();
        }

    });
</script> @endsection