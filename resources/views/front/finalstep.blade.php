@extends('layouts.CartMasterPage')
@section('SubCssCart')
<title>بیتکوین - تسویه نهایی</title>
    <style>


        a {
            color: #fff;
            text-decoration: none;
        }

        a:hover {
            color: #fff;
            text-decoration: none;
        }

        #closeBox {
            width: auto;
            height: auto;
        }
        .typeOfpayment{
            width:100%;
            height: 250px;
            border: 1px solid gray;
            box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5);
        }
        .marginTopAndRight{
            margin-top: 15px;
            margin-right: 30px;
        }
        .marginTopAndRight2{
            margin-top: 15px;
            margin-right: 17px;
        }
        .floatRight{
            float: right;
        }
        .marginTop13px{
            margin-top: 13px;
        }
        #creditCard{
            width:20px; height: auto; margin-top: 17px; margin-right: 20px;
        }
        .banksaman{
            width: 250px;
            height:55px;
            border-radius: 10px;
            border: 1px solid gray;
            margin-top:20px;
            margin-right:20px;

        }
        .zarrinpal{
            width: 250px;
            height:55px;
            border-radius: 10px;
            border: 1px solid gray;
            margin-top:20px;
            margin-right:20px;
        }
        .activeBank{
            background-color: #C3DFF7;
        }
        .samanLogo{
            margin-right: 35px;
            width:90px;
            height: auto;
        }
        .zarrinLogo{
            margin-right: 90px;
            width:40px;
            height: auto;
        }
        .zarrinpal input{
            border: 1px solid black!important;
        }
        .banksaman input{
            border: 1px solid black!important;
        }
        @media (max-width: 768px) {
            .typeOfpayment{
               height:300px!important;
            }
            #textPay{
                font-size:14px;
            }
            .viewProducts2 .row{
                margin:0 auto!important;
                /*background-color: red;*/
            }
            .viewProducts2 .row div{
                margin:0 auto!important;
                /*background-color: red;*/
            }

            .viewProducts2 .row img{
                margin:0 auto!important;
                margin-top: 10px!important;
            }
            .viewProducts2 .row h6{
                margin-right: 0px!important;
            }
        }
        .history{
            width:100%;
            height: auto;
            border: 1px solid gray;
            box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5);

        }
        .viewProducts{
            width:95%;
            height:auto;
            margin : 0 auto;
            background-color: rgba(108, 107, 107, 0.14);
            margin-top:20px;
            border:1px solid rgba(112, 112, 112, 0.37);
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;

        }
        .viewProducts2{
            width:95%;
            height:auto;
            margin-top: -1px !important;
            margin : 0 auto;
            /*background-color: rgba(108, 107, 107, 0.14);*/
            margin-top:20px;
            border:1px solid rgba(112, 112, 112, 0.37);
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            margin-bottom: 20px;
        }
        .imgPrc {
            width: 122px;
            height: 120px;
            margin-left: 5px;
            margin-top: 30px;
            margin-right: 15px;
            display: block;
            float: right;
            margin-bottom: 20px;
        }
        .viewProducts2 img{
            margin-right: 25px ;
        }
        .viewProducts2 h6{
            margin-right: 25px ;
            line-height: 25px;
            text-align: center;
        }
        .countDevice{
            margin-top:0;
            font-size: 13px;
        }
    </style>
@endsection
@section('SubMainCart')

        <div class="col-md-9 uk-margin-medium-bottom">
            <h4>انتخاب شیوه پرداخت</h4>
            <div class="typeOfpayment uk-margin-bottom">

                <div style="height: 70px;">
                    <div class="floatRight">
                        <div class="custom-control custom-radio marginTopAndRight">
                            <input type="checkbox" class="custom-control-input" id="customCheck1" checked="checked" disabled="disabled">
                            <label class="custom-control-label" for="customCheck1"></label>
                        </div>

                    </div>

                    <div class="floatRight" >
                        <img src="img/svg/creditCard.svg" alt="creditCard" id="creditCard">
                    </div>

                    <div class="floatRight">
                        <p class="uk-margin-right marginTop13px" id="textPay" >پرداخت اینترنتی(با تمامی کارت های بانکی)</p>
                        <p class="uk-text-meta uk-margin-right">سرعت بیشتر در ارسال و پردازش سفارش </p>
                    </div>
                </div>
                <hr>
                <div class="floatRight">
                    <form action="">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-lg-6">

                            <div class="banksaman activeBank">

                                <div class="custom-control custom-radio marginTopAndRight2">
                                    <input type="radio" class="custom-control-input" name="pay" id="customCheck2" checked>
                                    <label class="custom-control-label" for="customCheck2" id="labelAct2"><p>بانک سامان</p></label>
                                    <img class="samanLogo" src="https://www.sb24.com/images/icons/saman-logo.png">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-lg-6">
                            <div class="zarrinpal">
                                <div class="custom-control custom-radio marginTopAndRight2">
                                    <input type="radio" class="custom-control-input" name="pay" id="customCheck3" >
                                    <label  class="custom-control-label" for="customCheck3" id="labelAct3"><p>زرین پال</p></label>
                                    <img class="zarrinLogo" src="https://homepaz.com/static/images/zarinpal.svg">
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <h4>خلاصه سفارش</h4>
            <div class="history">
                <div class="viewProducts ">
                    <div class="row text-center uk-margin-top">

                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 uk-margin-small-bottom">
                            <div>
                                <p>مرسوله 5 از 5</p>
                                <p class="uk-text-meta uk-text-small">( پنج کالا)</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 uk-margin-small-bottom">
                            <div>
                                <p>زمان ارسال</p>
                                <p class="uk-text-meta uk-text-small">حداکثر تا 15 روز آینده</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 uk-margin-small-bottom">
                            <div>
                                <p>نحوه ارسال</p>
                                <p class="uk-text-meta uk-text-small">تحویل اکسپرسآی بیت</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 uk-margin-small-bottom">
                            <div>
                                <p>تاخیر در ارسال</p>
                                <p class="uk-text-meta uk-text-small">یک رو کاری</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 uk-margin-small-bottom">
                            <div>
                                <p>هرینه ارسال</p>
                                <p class="uk-text-primary uk-text-small">24,500 تومان</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 uk-margin-small-bottom">
                            <div>
                                <p class="text-success">آماده پرداخت</p>
                                <img src="img/svg/success.svg" alt="success" width="20" height="20" uk-svg>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="viewProducts2">
                    <div class="row">
                        <div class="">
                            <img src="img/mine.jpg" class="imgPrc" alt="">
                            <h6 class="uk-text-center@s">دستگاه ماینر مدل <br> ANTMN S9</h6>
                            <h6 class="countDevice uk-text-meta">تعداد : 2 </h6>
                        </div>
                        <div class="">
                            <img src="img/mine.jpg" class="imgPrc" alt="">
                            <h6>دستگاه ماینر مدل <br> ANTMN S9</h6>
                            <h6 class="countDevice uk-text-meta">تعداد : 2 </h6>
                        </div>
                        <div class="">
                            <img src="img/mine.jpg" class="imgPrc" alt="">
                            <h6>دستگاه ماینر مدل <br> ANTMN S9</h6>
                            <h6 class="countDevice uk-text-meta">تعداد : 2 </h6>
                        </div>
                        <div class="">
                            <img src="img/mine.jpg" class="imgPrc" alt="">
                            <h6>دستگاه ماینر مدل <br> ANTMN S9</h6>
                            <h6 class="countDevice uk-text-meta">تعداد : 2 </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
@section('SubScriptCart')
<script >
    $(document).ready(function () {
        $(".zarrinpal,#labelAct3").click(function () {
            $("#customCheck2").removeAttr("checked");
            $("#customCheck3").attr("checked","checked");
            $(".banksaman").removeClass("activeBank");
            $(".zarrinpal").addClass("activeBank");
        });
        $(".banksaman,#labelAct2").click(function () {
            $("#customCheck3").removeAttr("checked");
            $("#customCheck2").attr("checked","checked");
            $(".zarrinpal").removeClass("activeBank");
            $(".banksaman").addClass("activeBank");
        });
    })


</script>
@endsection

