<!doctype html>
<html lang="fa" dir="rtl">

<head>
    <title>بیتکوین</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="img/coin_Tg0_icon.ico" type="image/x-icon">

    <!-- en font -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <!-- Bootstrap rtl CSS -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css" integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz"
        crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/css/uikit.min.css" />


    <!-- style -->
    <style>
        body {
            cursor: default;
        }

        .Posts .Post:nth-child(1) {
            margin-top: 40px !important;

        }

        .Posts .Post {
            border-radius: 10px;
            border-top-left-radius: 40px;
            overflow: hidden;
            box-shadow: 0px 2px 3px #00000062 !important;
            border-top: 0.5px solid #00000010;
            height:auto;

        }

        .Posts .Post span,
        .Posts .Post button {
            transition: 300ms;
            cursor: pointer;

        }

        .Posts .Post .btnHover:hover {
            box-shadow: 0px 2px 10px #00000012 !important;
            color: #1e87f0;
            background-color: #fff;
        }

        .Posts .Post span:hover {
            text-shadow: 0px 2px 10px #00000012 !important;
            color: #1e87f0;
        }

        .Posts .Post .PostBoxs img {
            width: 100%;
            max-height: 420px;

        }

        .Posts .Post .TextPost {
            width: 100%;
            height: auto;
            padding-bottom: 0px !important ;
        }
        .Posts .Post .TextPost text{

            height: auto;
        }

        .Posts .Post .TextPost .btnPost {
            margin-top: 60px;
            margin-bottom: 35px;


        }

        .Posts .Post .TextPost .btnPost .btnContinue {
            float: left;
            border-radius: 3px;
        }

        .Posts .Post .TextPost .TitlePost {
            height: 80px;
        }

        .Posts .Post .TextPost .TitlePostRight,
        .Posts .Post .TextPost .TitlePostRight h3 {
            float: right;
            font-size: 1rem !important;
            color: #000 !important;
            line-height: 5px !important;
            cursor: default;

        }

        .Posts .Post .TextPost .TitlePostRight h3 {
            font-size: 1.5rem !important;
            color: #000 !important;
            line-height: 20px !important;
        }

        .Posts .Post .TextPost .TitlePostLeft {
            float: left;

        }

        .Posts .Post .TextPost .TitlePostLeft span {
            border-radius: 3px;

        }

        .Posts .Post .TextPost .TitlePostLeft span {
            margin-right: 5px;
        }

        .Posts .Post .PostBoxs {
            padding: 0px;
            overflow: hidden;
        }
        .Posts .Post .PostBoxs img{
            float: left ;
        }
    </style>

</head>

<body class="">
    <main>
        <div class="container-fluid">
            <div class="Posts" id="posts">
                {{--<div class="Post uk-margin-medium-bottom uk-background-muted">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 PostBoxs">--}}
                            {{--<img class="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCbzMo16-av29MtGK9AydNtS6vS-ztWXRdPgGW6Cmvxw6woUjgHg" alt=""--}}
                                {{--srcset="">--}}
                        {{--</div>--}}
                        {{--<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 PostBoxs">--}}
                            {{--<div class="TextPost uk-padding-large">--}}
                                {{--<div class="TitlePost">--}}
                                    {{--<div class="TitlePostRight">--}}
                                        {{--<h3>نام ادمین</h3>--}}
                                        {{--<p class="uk-text-meta">توسعه</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="TitlePostLeft">--}}
                                        {{--<span class="uk-label btnHover">تست</span>--}}
                                        {{--<span class="uk-label btnHover">بیتکوین</span>--}}
                                        {{--<span class="uk-label btnHover">بلاگ</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="text">--}}
                                    {{--<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Distinctio quam ex hic sunt--}}
                                        {{--fugiat magni cum nesciunt libero aliquid error quos, itaque consequuntur illum aspernatur--}}
                                        {{--neque iure placeat amet repellendus amet consectetur, adipisicing elit. Distinctio quam ex hic sunt--}}
                                        {{--fugiat magni cum nesciunt libero aliquid error quos, itaque consequuntur illum aspernatur--}}
                                        {{--neque iure placeat amet repelle.</p>--}}
                                {{--</div>--}}
                                {{--<div class="btnPost">--}}
                                    {{--<button class="uk-button uk-button-primary uk-button-small btnContinue  btnHover">ادامه مطلب</button>--}}
                                    {{--<span class="uk-margin-small-right btnSocial" uk-icon="twitter"></span>--}}
                                    {{--<span class="uk-margin-small-right btnSocial" uk-icon="instagram"></span>--}}
                                    {{--<span class="uk-margin-small-right btnSocial" uk-icon="facebook"></span>--}}
                                    {{--<span class="uk-margin-small-right btnSocial" uk-icon="world"></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="Post uk-margin-medium-bottom uk-background-muted">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 PostBoxs">--}}
                            {{--<img class="img-fluid" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIANgAWgMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAACAwABBAUGB//EADwQAAIBAgUCAwMHCwUAAAAAAAABAhESAwQTIWEFURQxQQZSkRUiQmKSodEyM0NTcoGCscHw8SMkNGNx/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAbEQEBAQEAAwEAAAAAAAAAAAAAEQESAiEiYf/aAAwDAQACEQMRAD8A+ZqAagNWGi1HY90c6UocFqNR0YteQVu+4gSodgrNhygFavUsGexc19CrDQ4pvaqdCSi0qNb9ywZpRFyianEBxJpWVxBtXY0OO5VhmDQoMJRaGNbum6rtXzLtNoBR9QqJxp6phKIUoOMnGSo06NcgAkXaEkFaULpXuSTbUklSvmMUaAtbcgJmn3BceB7j6rsA0QIcezBtfI9oGnCIrSolqI20lpqMlpFpbB2l0LACiXaMUQlEpSrdgJIfJAuI3CkUBaHNAOJIUlorca0VQyVroSg20q02gLSWjKMloApBULUQmqIqFtANDWtqgNBSmgWhzW3kC4kCGiqDWirSQbnEqg20q0qFULoHaWolAULtGKJTTAU4g2jnEFxAVKNsmtnTbbyAaHyjuA41AU0DQc4lWkVvcSrTQ4AuICLeCWjrS7ShSiSz1HKBHDYBDXADiPtYLgVGdxBtNDiA4ECbSrR9hLSK6bw+AbDW4g2cEqxl0y7DRpksKhCgDKO5qs4BlADK4guJpcAXADK4FOBpcCaYRmsJYatMmmSq6rwitI52W9ruiY9LsxPBk/o4uG1T96qjWut9Gl5dUyf78ZI59OkMeGTTGYOayWYp4fN5fFr5WYsXX7zRpelBSMdgEsKozM5vK5eupiwqvNJnHzPWVOFcvNRXnRJbl6I6Tw9gdMyZXreDfpZ1xhKitxV+TLng6tilFSi1KL8mnsy1IyaZFhmvTRawtxUjJpE0jdpE0iUj5BKC7r4ipJLzOxLof/ZECXRV76Oc06xxm0nVUr3HR6nnYK2GezkY+7HHml8KnQfRvrxBfSGv0kCTWrjnyz+Ze7xpy/adQl1LGi0614Nj6T3xIAvpK96JPo9aU+qpxo8Obf7WwzI+0nUMhL/aYlsP1cvnR+D/AKE+SkvpxK+S1+sgL5np14e3vUUvnZXKvbtLz+IjH9tetYr/ANPEwcCPbDwk/wCdTB8mJfTiU+nU/SRM/S+jsT2q67ixUX1CaS8rIRi39xil1XqspNvqWeq3X/kT/Ef4D68SeA+vEk8luNvjpdwHnZd38R+H0ZL87mfsR/EL5Gy/rmMT4I7XXPln8Y36isTNSpszp4PTMlhu53z4nLb7jQsvkqUWXw/sj2cvPeKn3qX4jE5Otj5HBlvhJLgxYmCsNO7zMrGd4mJ/bBvxnJRjFtv0RrwstqypRKC85P1Oll44WWVMKKT971EpHKWXz2yeXxK/+AYsMzhfnMHEj/CzveI5LWZ5E/WnnVizo/my24K1+T0fiEwdTD92PwHKsbzHJXiOTmvGJq8jpmOjr8kWPyc1YpNXkUjo69fUGWNX8pRdO6MGryU8Vikb1jpJJeRTx+TDqlLEbdF5iq36/JNfk56xSapKOhr8k8Ryc/VJqCqzanJNQzahLzFRp1CX8mbULvFGi/kjxDPeU5ijReVfyIuKvCtF5LzPeS8DReS/kzXhXPsQZ7yXme4tSJQ+8tTq6dzPcS4o0Xl3Ge4K+oDXP/JJTVW0qLsIuCi7qJNKrpVvYBik3Wibpu6ehV4pTotqqpVxA68l3IhyJcxVLqSpCGBC6kIBdSVIQuCVJUhAJUqpCE0UyVIQD//Z" alt=""--}}
                                 {{--srcset="">--}}
                        {{--</div>--}}
                        {{--<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 PostBoxs">--}}
                            {{--<div class="TextPost uk-padding-large">--}}
                                {{--<div class="TitlePost">--}}
                                    {{--<div class="TitlePostRight">--}}
                                        {{--<h3>نام ادمین</h3>--}}
                                        {{--<p class="uk-text-meta">توسعه</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="TitlePostLeft">--}}
                                        {{--<span class="uk-label btnHover">تست</span>--}}
                                        {{--<span class="uk-label btnHover">بیتکوین</span>--}}
                                        {{--<span class="uk-label btnHover">بلاگ</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="text">--}}
                                    {{--<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Distinctio quam ex hic sunt--}}
                                        {{--fugiat magni cum nesciunt libero aliquid error quos, itaque consequuntur illum aspernatur--}}
                                        {{--neque iure placeat amet repellendus amet consectetur, adipisicing elit. Distinctio quam ex hic sunt--}}
                                        {{--fugiat magni cum nesciunt libero aliquid error quos, itaque consequuntur illum aspernatur--}}
                                        {{--neque iure placeat amet repelle.</p>--}}
                                {{--</div>--}}
                                {{--<div class="btnPost">--}}
                                    {{--<button class="uk-button uk-button-primary uk-button-small btnContinue  btnHover">ادامه مطلب</button>--}}
                                    {{--<span class="uk-margin-small-right btnSocial" uk-icon="twitter"></span>--}}
                                    {{--<span class="uk-margin-small-right btnSocial" uk-icon="instagram"></span>--}}
                                    {{--<span class="uk-margin-small-right btnSocial" uk-icon="facebook"></span>--}}
                                    {{--<span class="uk-margin-small-right btnSocial" uk-icon="world"></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}


            </div>
        </div>

        </div>
        </div>
    </main>




    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://cdn.rtlcss.com/bootstrap/v4.0.0/js/bootstrap.min.js" integrity="sha384-54+cucJ4QbVb99v8dcttx/0JRx4FHMmhOWi4W+xrXpKcsKQodCBwAvu3xxkZAwsH"
        crossorigin="anonymous"></script>

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit-icons.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>



    <!-- script -->
    <script>
        $(document).ready(function () {

            $.get("/clientshowallpost",function (data,status) {

                $.each(data.posts, function (i,e) {

                  $('.Posts').append(' <div class="Post uk-margin-medium-bottom uk-background-muted">\n' +
                      '                    <div class="row">\n' +
                      '                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 PostBoxs">\n' +
                      '                            <img class="img-fluid" src="/'+e.avatar_image_path+'" alt=""\n' +
                      '                                srcset="">\n' +
                      '                        </div>\n' +
                      '                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 PostBoxs">\n' +
                      '                            <div class="TextPost uk-padding-large">\n' +
                      '                                <div class="TitlePost">\n' +
                      '                                    <div class="TitlePostRight">\n' +
                      '                                       <a target="_blank" href="/blog/post/'+e.id+'"> <h3>'+e.title+'</h3></a> \n' +
                      '                                        <p class="uk-text-meta">'+e.user_name+'</p>\n' +
                      '                                    </div>\n' +
                      '                                    <div class="TitlePostLeft">\n' +
                      '                                        <span class="uk-label btnHover">تست</span>\n' +
                      '                                        <span class="uk-label btnHover">بیتکوین</span>\n' +
                      '                                        <span class="uk-label btnHover">بلاگ</span>\n' +
                      '                                    </div>\n' +
                      '                                </div>\n' +
                      '                                <div class="text">\n' +
                      '                                    <p>'+e.description+'</p>\n' +
                      '                                </div>\n' +
                      '                                <div class="btnPost">\n' +
                      '                                     <a target="_blank" href="/blog/post/'+e.id+'"> <button class="uk-button uk-button-primary uk-button-small btnContinue  btnHover">ادامه مطلب</button></a>\n' +
                      '                                    <span class="uk-margin-small-right btnSocial" uk-icon="twitter"></span>\n' +
                      '                                    <span class="uk-margin-small-right btnSocial" uk-icon="instagram"></span>\n' +
                      '                                    <span class="uk-margin-small-right btnSocial" uk-icon="facebook"></span>\n' +
                      '                                    <span class="uk-margin-small-right btnSocial" uk-icon="world"></span>\n' +
                      '                                </div>\n' +
                      '                            </div>\n' +
                      '                        </div>\n' +
                      '                    </div>\n' +
                      '                </div>');

                });

            });

        });

    </script>
</body>

</html>