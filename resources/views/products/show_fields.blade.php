
<!-- Product Type Id Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('product_type_id', 'Product Type Id:') !!}
    <p>{!! $product->product_type_id !!}</p>
</div>

<!-- Market Id Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('market_id', 'Market Id:') !!}
    <p>{!! $product->market_id !!}</p>
</div>

<!-- Special Service Id Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('special_service_id', 'Special Service Id:') !!}
    <p>{!! $product->special_service_id !!}</p>
</div>

<!-- Rank Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('rank', 'Rank:') !!}
    <p>{!! $product->rank !!}</p>
</div>

<!-- Weight Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('weight', 'Weight:') !!}
    <p>{!! $product->weight !!}</p>
</div>

<!-- Off Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('off', 'Off:') !!}
    <p>{!! $product->off !!}</p>
</div>

<!-- Name Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $product->name !!}</p>
</div>

<!-- Dimensions Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('dimensions', 'Dimensions:') !!}
    <p>{!! $product->dimensions !!}</p>
</div>

<!-- Colors Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('colors', 'Colors:') !!}
    <p>{!! $product->colors !!}</p>
</div>

<!-- Gas Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('gas', 'Gas:') !!}
    <p>{!! $product->gas !!}</p>
</div>

<!-- Main Description Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('main_description', 'Main Description:') !!}
    <p>{!! $product->main_description !!}</p>
</div>

<!-- Sub Description Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('sub_description', 'Sub Description:') !!}
    <p>{!! $product->sub_description !!}</p>
</div>

<!-- Status Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $product->status !!}</p>
</div>

<!-- Poweruse Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('poweruse', 'Poweruse:') !!}
    <p>{!! $product->poweruse !!}</p>
</div>

<!-- Btc Per Hour Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('btc_per_hour', 'Btc Per Hour:') !!}
    <p>{!! $product->btc_per_hour !!}</p>
</div>

<!-- Price Per Unit Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('price_per_unit', 'Price Per Unit:') !!}
    <p>{!! $product->price_per_unit !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $product->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $product->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $product->updated_at !!}</p>
</div>

