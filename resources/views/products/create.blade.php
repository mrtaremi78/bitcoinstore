@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Product
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'products.store' , 'files'=>'true']) !!}

                        @include('products.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

{{--@section('scripts')--}}

    {{--<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>--}}
    {{----}}
{{--@endsection--}}
