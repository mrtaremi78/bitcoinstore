<table class="table table-responsive" id="shipments-table">
    <thead>
        <tr>
            <th>Postman Id</th>
        <th>Shipmenttype Id</th>
        <th>Transaction Id</th>
        <th>Send Date</th>
        <th>Delivered Date</th>
        <th>Send Address</th>
        <th>Delivered Address</th>
        <th>Description</th>
        <th>Post Code</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($shipments as $shipment)
        <tr>
            <td>{!! $shipment->postman_id !!}</td>
            <td>{!! $shipment->shipmenttype_id !!}</td>
            <td>{!! $shipment->transaction_id !!}</td>
            <td>{!! $shipment->send_date !!}</td>
            <td>{!! $shipment->delivered_date !!}</td>
            <td>{!! $shipment->send_address !!}</td>
            <td>{!! $shipment->delivered_address !!}</td>
            <td>{!! $shipment->description !!}</td>
            <td>{!! $shipment->post_code !!}</td>
            <td>
                {!! Form::open(['route' => ['shipments.destroy', $shipment->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('shipments.show', [$shipment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('shipments.edit', [$shipment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>