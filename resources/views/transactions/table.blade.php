<table class="table table-responsive" id="transactions-table">
    <thead>
        <tr>
            <th>User Id</th>
        <th>Paymenttype Id</th>
        <th>Total Prices</th>
        <th>Status</th>
        <th>Province Id</th>
        <th>City Id</th>
        <th>Neighbourhood Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($transactions as $transaction)
        <tr>
            <td>{!! $transaction->user_id !!}</td>
            <td>{!! $transaction->paymenttype_id !!}</td>
            <td>{!! $transaction->total_prices !!}</td>
            <td>{!! $transaction->status !!}</td>
            <td>{!! $transaction->province_id !!}</td>
            <td>{!! $transaction->city_id !!}</td>
            <td>{!! $transaction->Neighbourhood_id !!}</td>
            <td>
                {!! Form::open(['route' => ['transactions.destroy', $transaction->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('transactions.show', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('transactions.edit', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>