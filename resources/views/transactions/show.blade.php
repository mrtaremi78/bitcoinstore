@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Transaction
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('transactions.show_fields')
                    <a href="{!! route('transactions.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>

        </div>

        <div class="box box-primary">
            <table class="table table-responsive" id="allsells-table">
                <thead>
                <tr>
                    <th>Product Id</th>
                    <th>Transaction Id</th>
                    <th>Count</th>
                    <th colspan="3">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($allsells as $allsell)
                    <tr>
                        <td>{!! $allsell->product_id !!}</td>
                        <td>{!! $allsell->transaction_id !!}</td>
                        <td>{!! $allsell->count !!}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{!! route('allsells.show', [$allsell->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box box-primary">
            <table class="table table-responsive" id="shipments-table">
                <thead>
                <tr>
                    <th>Shipmenttype Id</th>
                    <th>Send Date</th>
                    <th>Delivered Date</th>
                    <th colspan="3">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($shipments as $shipment)
                    <tr>
                        <td>{!! $shipment->shipmenttype_id !!}</td>
                        <td>{!! $shipment->send_date !!}</td>
                        <td>{!! $shipment->delivered_date !!}</td>
                        <td>
                            <a href="{!! route('shipments.show', [$shipment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
