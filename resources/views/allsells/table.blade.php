<table class="table table-responsive" id="allsells-table">
    <thead>
        <tr>
            <th>Product Id</th>
        <th>Transaction Id</th>
        <th>Count</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($allsells as $allsell)
        <tr>
            <td>{!! $allsell->product_id !!}</td>
            <td>{!! $allsell->transaction_id !!}</td>
            <td>{!! $allsell->count !!}</td>
            <td>
                {!! Form::open(['route' => ['allsells.destroy', $allsell->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('allsells.show', [$allsell->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('allsells.edit', [$allsell->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>