<table class="table table-responsive" id="comments-table">
    <thead>
        <tr>
            <th>Post Id</th>
        <th>Product Id</th>
        <th>Reply Of Comment</th>
        <th>Likes</th>
        <th>Value</th>
        <th>Accepted</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($comments as $comment)
        <tr>
            <td>{!! $comment->post_id !!}</td>
            <td>{!! $comment->product_id !!}</td>
            <td>{!! $comment->reply_of_comment !!}</td>
            <td>{!! $comment->likes !!}</td>
            <td>{!! $comment->value !!}</td>
            <td>{!! $comment->accepted !!}</td>
            <td>
                {!! Form::open(['route' => ['comments.destroy', $comment->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('comments.show', [$comment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('comments.edit', [$comment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>