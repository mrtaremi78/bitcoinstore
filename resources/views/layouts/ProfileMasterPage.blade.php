@extends('layouts.MasterPage') @section('SubCss')
<!-- master style profile -->
<link rel="stylesheet" href="/css/ProfileStyle.css">
<!-- master style profile -->
@yield('SubCssProfile') @endsection @section('SubModal')
<div id="Sign-out-Modal" class="uk-margin-medium-top" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <br>
        <div class="">
            <img class="imgDanger" src="/img/svg/danger.svg" alt="" uk-svg>
        </div>
        <div class="uk-modal-body">
            <p class="uk-margin-top text-center ">
                آیا میخواهید ار خساب خود خارج شوید؟
            </p>
        </div>
        <div class="uk-modal-footer uk-text-right   ">
            <button class="uk-button uk-button-default uk-modal-close" type="button">انصراف</button>
            <button class="uk-button uk-button-danger" type="button">خروج</button>
        </div>
    </div>
</div>
{{--
<div id="Change-Password-Modal" class="uk-margin-top" uk-modal>
    <div class="uk-modal-dialog">

        <button class="uk-modal-close-default" type="button" uk-close></button>

        <div class="uk-modal-header">
            <p class="uk-modal-title uk-margin-top text-center" style="font-size: 25px;">تغییر رمز</p>
        </div>

        <div class="uk-modal-body">
            <form action="" class="form-group">
                <div class="row">

                    <div class="col-md-8 offset-md-2">
                        <label class="uk-margin-small-bottom">رمز عبور جدید:</label>
                        <div class="uk-margin-bottom uk-inline">

                            <span class="uk-form-icon uk-form-icon-flip bg-span">
                                <img src="/img/svg/lock.svg" width="20" height="20" alt="" uk-svg>
                            </span>
                            <input type="password" class="uk-input uk-form-width-large">
                        </div>

                        <label class="uk-margin-small-bottom"> تکرار رمز عبور جدید:</label>
                        <div class="uk-margin-bottom uk-inline">

                            <span class="uk-form-icon uk-form-icon-flip bg-span">
                                <img src="/img/svg/lock-open.svg" width="20" height="20" alt="" uk-svg>
                            </span>
                            <input type="password" class="uk-input uk-form-width-large">
                        </div>
                    </div>

                </div>
            </form>
        </div>

        <div class="uk-modal-footer uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">انصراف</button>
            <button class="uk-button uk-label-success" type="button">ثبت</button>
        </div>

    </div>
</div> --}}
@yield('SubModalProfile')
@endsection
@section('SubMain')
<div class="container-fluid">

    <div class="row">
        <div class="col-md-3 text-center">
            <div class="boxProf">
                <div class="colorStatus"></div>
                <div class="imgProf">
                    <div class="edit">
                        <img src="/img/avatar.jpg" alt="imgProf">
                    </div>
                    <div id="iconEdit">
                        <img src="/img/svg/pencil.svg" width="15" height="15" alt="">
                    </div>
                    <h5 class=" uk-margin-top">زهرا احمدی</h5>
                </div>
                <div class="buttonAction">

                    <div class="uk-button-group uk-width-1-1">
                        <a class="uk-button uk-width-1-1 borderButton" href="/profile/changePassword">

                            <span>
                                <img src="/img/svg/lock-open.svg" alt="" uk-svg width="20" height="20" id="shopIconMargin">
                            </span>
                            <br> تغییر رمز
                        </a>
                        <a class="uk-button uk-width-1-1 borderButton" href="#Sign-out-Modal">

                            <span>
                                <img src="/img/svg/sign-out.svg" alt="" uk-svg width="20" height="20" id="shopIconMargin">
                            </span>
                            <br> خروج از حساب
                        </a>
                    </div>
                </div>
            </div>


            <div class="redirect uk-margin-top">
                <div id="headerRedirect">
                    <h5>حساب کاربری<span id="triangle-down" uk-icon="icon: triangle-down"></span></h5>
                </div>
                <div class="items">
                    <a href="/profile">
                        <div class="item text-center" id="profile">
                            <img src="/img/svg/user.svg" width="15" height="15" alt="" srcset="">
                            <p>پروفایل</p>
                        </div>
                    </a>
                    <a href="/profile/bookmark">
                        <div class="item" id="bookmark">
                            <img src="/img/svg/bookmark.svg" width="15" height="15" alt="" srcset="">
                            <p>لیست علاقه مندی</p>
                        </div>
                    </a>
                    <a href="/profile/address">
                        <div class="item" id="address">
                            <img src="/img/svg/location.svg" width="15" height="15" alt="" srcset="">
                            <p>آدرس ها</p>
                        </div>
                    </a>
                    <a href="/profile/information">
                        <div class="item" id="information">
                            <img src="/img/svg/info-square.svg" width="15" height="15" alt="" srcset="">
                            <p>اطلاعات شخصی</p>
                        </div>
                    </a>
                    <a href="/profile/order">
                        <div class="item" id="order">
                            <img src="/img/svg/tag.svg" width="15" height="15" alt="" srcset="">
                            <p>سفارشات</p>
                        </div>
                    </a>
                    <a href="/profile/question">
                        <div class="item" id="question">
                            <img src="/img/svg/comments.svg" width="15" height="15" alt="" srcset="">
                            <p>پرسش و پاسخ</p>
                        </div>
                    </a>
                    <a href="/profile/requestreturn">
                        <div class="item" id="requestreturn">
                            <img src="/img/svg/gift-card.svg" width="15" height="15" alt="" srcset="">
                            <p>درخواست مرجوعی</p>
                        </div>
                    </a>

                </div>
            </div>







        </div>

        <div class="col-md-9">
            <div class="row">
                @yield('SubMainProfile')
            </div>
        </div>
    </div>


    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
</div>
@endsection @section('SubScript')
<!-- master script profile -->
<script src="/js/ProfileScript.js"></script>
<!-- master script profile -->
@yield('SubScriptProfile') @endsection