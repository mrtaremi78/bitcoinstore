<li class="{{ Request::is('products*') ? 'active' : '' }}">
    <a href="{!! route('products.index') !!}"><i class="fa fa-edit"></i><span>Products</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>Roles</span></a>
</li>

<li class="{{ Request::is('shipments*') ? 'active' : '' }}">
    <a href="{!! route('shipments.index') !!}"><i class="fa fa-edit"></i><span>Shipments</span></a>
</li>

<li class="{{ Request::is('postmen*') ? 'active' : '' }}">
    <a href="{!! route('postmen.index') !!}"><i class="fa fa-edit"></i><span>Postmen</span></a>
</li>

<li class="{{ Request::is('comments*') ? 'active' : '' }}">
    <a href="{!! route('comments.index') !!}"><i class="fa fa-edit"></i><span>Comments</span></a>
</li>

<li class="{{ Request::is('images*') ? 'active' : '' }}">
    <a href="{!! route('images.index') !!}"><i class="fa fa-edit"></i><span>Images</span></a>
</li>

<li class="{{ Request::is('shipmentTypes*') ? 'active' : '' }}">
    <a href="{!! route('shipmentTypes.index') !!}"><i class="fa fa-edit"></i><span>Shipment Types</span></a>
</li>

<li class="{{ Request::is('paymentTypes*') ? 'active' : '' }}">
    <a href="{!! route('paymentTypes.index') !!}"><i class="fa fa-edit"></i><span>Payment Types</span></a>
</li>

<li class="{{ Request::is('productTypes*') ? 'active' : '' }}">
    <a href="{!! route('productTypes.index') !!}"><i class="fa fa-edit"></i><span>Product Types</span></a>
</li>

<li class="{{ Request::is('transactions*') ? 'active' : '' }}">
    <a href="{!! route('transactions.index') !!}"><i class="fa fa-edit"></i><span>Transactions</span></a>
</li>

<li class="{{ Request::is('allsells*') ? 'active' : '' }}">
    <a href="{!! route('allsells.index') !!}"><i class="fa fa-edit"></i><span>Allsells</span></a>
</li>

<li class="{{ Request::is('posts*') ? 'active' : '' }}">
    <a href="{!! route('posts.index') !!}"><i class="fa fa-edit"></i><span>Posts</span></a>
</li>

<li class="{{ Request::is('postTags*') ? 'active' : '' }}">
    <a href="{!! route('postTags.index') !!}"><i class="fa fa-edit"></i><span>Post Tags</span></a>
</li>


