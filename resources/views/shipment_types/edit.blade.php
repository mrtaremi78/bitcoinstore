@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Shipment Type
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($shipmentType, ['route' => ['shipmentTypes.update', $shipmentType->id], 'method' => 'patch']) !!}

                        @include('shipment_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection